<!--
SPDX-FileCopyrightText: 2018 Michael Pöhn <michael@poehn.at>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [0.6.0] - 2020-06-10
### Added
- support for bootstrapping buster images

### Changed
- conform to [reuse.software](https://reuse.software)

### Fixed
- use utf-8 encoding for reading/writing all files

### Removed
- discontinued jessie support

## [0.5.1] - 2019-01-28
### Added
- added CHANGELOG.md

### Changed
- improved documentation about using boxes with makebuildserveri in readme

## [0.5] - 2018-09-21
### Changed
- set defualt locale *en_GB.UTF-8* and made sure *en_US.UTF-8* is available

### Fixed
- make sure apt https support is pre-installed

## [0.4] - 2018-09-16
### Added
- added note about vagrant hosted pre-build images to readme
- added some dry-run test cases

### Changed
- renamed output boxes to eg. basebox-stretch64-virtualbox.box

### Fixed
- make _--dry-run_ work without root
- fixed mention of _makebuildserver_ in readme

## [0.3] - 2018-09-11
### Changed
- default to building stretch

## [0.2] - 2018-09-08
### Changed
- re-named script to `fdroid_basebox.py`

## [0.1.1] - 2018-09-04
### Fixed
- Fixed return code when encountering an exception

## [0.1] - 2018-07-27
### Added
- support for building vagrant libvirt box
- support for building vagrant virtualbox box
- support for bootstrapping jessie
- support for bootstrapping stretch

[Unreleased]: https://gitlab.com/fdroid/basebox/compare/0.6.0...master
[0.5.1]: https://gitlab.com/fdroid/basebox/compare/0.5.1...0.6.0
[0.5.1]: https://gitlab.com/fdroid/basebox/compare/0.5...0.5.1
[0.5]: https://gitlab.com/fdroid/basebox/compare/0.4...0.5
[0.4]: https://gitlab.com/fdroid/basebox/compare/0.3...0.4
[0.3]: https://gitlab.com/fdroid/basebox/compare/0.2...0.3
[0.2]: https://gitlab.com/fdroid/basebox/compare/0.1.1...0.2
[0.1.1]: https://gitlab.com/fdroid/basebox/compare/0.1...0.1.1
