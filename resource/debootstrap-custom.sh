#! /bin/sh

# SPDX-FileCopyrightText: 2018 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -e
VMROOT=$1
PROVIDER="{provider}"
DEB_DISTRO="{deb_distro}"
USERNAME="{username}"
USERHOME="$VMROOT/home/$USERNAME"
echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> $VMROOT/etc/sudoers

VGRNT_UID=`ls -ln $VMROOT/home | grep $USERNAME | awk '{{print $3}}'`
VGRNT_GID=`ls -ln $VMROOT/home | grep $USERNAME | awk '{{print $4}}'`
mkdir $VMROOT/home/$USERNAME/.ssh
chmod 700 $VMROOT/home/$USERNAME/.ssh
chown $VGRNT_UID:$VGRNT_GID $USERHOME/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant_insecure_public_key" > $USERHOME/.ssh/authorized_keys
chmod 600 $USERHOME/.ssh/authorized_keys
chown $VGRNT_UID:$VGRNT_GID $USERHOME/.ssh/authorized_keys

# run locale-gen (has not effect, but do it anyway since that's what's documented on debian wiki)
echo "en_GB.UTF-8 UTF-8" >> $VMROOT/etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> $VMROOT/etc/locale.gen
if [ "{verbose}" = 'True' ]; then
    chroot $VMROOT locale-gen
else
    chroot $VMROOT locale-gen > /dev/null
fi
# set system wide locale to the same value as on the legacy vm image
echo "LANG=en_GB.UTF-8" > $VMROOT/etc/default/locale


echo "deb http://deb.debian.org/debian $DEB_DISTRO-backports main contrib" > $VMROOT/etc/apt/sources.list.d/$DEB_DISTRO-backports.list

if [ $PROVIDER = 'virtualbox' -a $DEB_DISTRO = 'stretch' ]; then

    # hack dns settings
    # TODO: find out how nspawn supports networking
    systemd-nspawn -D $VMROOT /usr/bin/unlink /etc/resolv.conf
    systemd-nspawn -D $VMROOT /bin/bash -c 'echo "nameserver 1.1.1.1" > /etc/resolv.conf'

    systemd-nspawn -D $VMROOT /usr/bin/apt-get update -y
    systemd-nspawn -D $VMROOT /usr/bin/apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils linux-headers-amd64

    # restore hacked dns settings
    systemd-nspawn -D $VMROOT /bin/bash -c 'unlink /etc/resolv.conf && ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf'

elif [ $PROVIDER = 'virtualbox' -a $DEB_DISTRO = 'buster' ]; then

    # hack dns settings
    # TODO: find out how nspawn supports networking
    systemd-nspawn -D $VMROOT /usr/bin/unlink /etc/resolv.conf
    systemd-nspawn -D $VMROOT /bin/bash -c 'echo "nameserver 1.1.1.1" > /etc/resolv.conf'

    systemd-nspawn -D $VMROOT /usr/bin/apt-get update -y
    systemd-nspawn -D $VMROOT /usr/bin/apt-get install -y gnupg

    cp resource/lucas-nussbaum.pub $VMROOT/root/lucas-nussbaum.pub
    systemd-nspawn -D $VMROOT /usr/bin/apt-key add /root/lucas-nussbaum.pub
    rm $VMROOT/root/lucas-nussbaum.pub
    echo "deb https://people.debian.org/~lucas/virtualbox-buster/ ./" > $VMROOT/etc/apt/sources.list.d/vbox.list

    systemd-nspawn -D $VMROOT /usr/bin/apt-get update -y
    systemd-nspawn -D $VMROOT /usr/bin/apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils linux-headers-amd64

    # restore hacked dns settings
    systemd-nspawn -D $VMROOT /bin/bash -c 'unlink /etc/resolv.conf && ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf'

fi
