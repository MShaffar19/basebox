#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2018 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import inspect
import logging
import optparse
import os
import sys
import unittest
import tempfile

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())),
                 '..'))
print('localmodule: ' + localmodule)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)

import fdroid_basebox


class MkBsBoxTest(unittest.TestCase):
    '''tests fdroid_basebox.py'''

    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        self.basedir = os.path.join(localmodule, 'tests')
        self.tmpdir = os.path.abspath(os.path.join(self.basedir, '..',
                                                   '.testfiles'))
        if not os.path.exists(self.tmpdir):
            os.makedirs(self.tmpdir)
        os.chdir(self.basedir)

    def test_write_bootstrap_script(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            path = os.path.join(tmpdir, 'script.sh')
            params = {'dry_run': False,
                      'boostrapscript': path,
                      'deb_distro': 'stretch',
                      'deb_mirror': 'http://deb.debian.org/debian',
                      'username': 'theUserName',
                      'verbose': True}
            fdroid_basebox.write_bootstrap_script(params, 'libvirt')

            self.assertTrue(os.path.isfile(path))
            self.assertTrue(os.access(path, os.X_OK))

    def test_main_dry_run(self):
        fdroid_basebox.main(dry_run=True, skip_checks=True, workdir='.')

    def test_main_dry_run_stretch64_virtualbox(self):
        fdroid_basebox.main(dry_run=True,
                            skip_checks=True,
                            workdir='.',
                            provider='virtualbox',
                            debver='stretch64')

    def test_main_dry_run_stretch64_libvirt(self):
        fdroid_basebox.main(dry_run=True,
                            skip_checks=True,
                            workdir='.',
                            provider='libvirt',
                            debver='stretch64')

    def test_main_dry_run_bad_provider(self):
        self.assertRaisesRegex(fdroid_basebox.BaseboxException,
                               "provider 'hyper-v' not supported. "
                               "Supported providers are: libvirt, virtualbox",
                               fdroid_basebox.main,
                               provider='hyper-v',
                               dry_run=True,
                               skip_checks=True)

    def test_main_dry_run_bad_debver(self):
        self.assertRaisesRegex(fdroid_basebox.BaseboxException,
                               "target debian version 'etch32' not supported. "
                               "Supported versions are: stretch64",
                               fdroid_basebox.main,
                               debver='etch32',
                               dry_run=True,
                               skip_checks=True)


if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option("-v", "--verbose", action="store_true", default=False,
                      help="Spew out even more information than normal")
    options, args = parser.parse_args(['--verbose'])

    newSuite = unittest.TestSuite()
    newSuite.addTest(unittest.makeSuite(MkBsBoxTest))
    unittest.main(failfast=False)
