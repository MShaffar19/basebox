#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2018 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import re
import os
import json
import math
import stat
import shutil
import logging
import tarfile
import tempfile
import subprocess
from argparse import ArgumentParser


SUPPORTED_PROVIDERS = ('libvirt', 'virtualbox')
SUPPORTED_DEBVERS = ('stretch64', 'buster64')


class BaseboxException(Exception):
    pass


def vm_size_str_to_bytes(size):
    ssize = str(size)
    if re.match('[0-9]+G', ssize):
        return int(float(ssize[:-1]) * (2**30))
    if re.match('[0-9]+M', ssize):
        return int(float(ssize[:-1]) * (2**20))
    raise BaseboxException("Could not convert, size '{}' to bytes. "
                           "(Try something like: '100G')".format(ssize))


def init_params(provider, debver, workdir='.', verbose=False, dry_run=False):
    """creates dict with carefully chosen parameters for buildservers"""

    params = {'vm_name': 'basebox-{}'.format(debver),
              'hostname': 'basebox-{}'.format(debver),
              'vm_ram': 1024,
              'vm_cpu': 1,
              'size': '1000G',
              'deb_mirror': 'http://deb.debian.org/debian',
              'deb_distro': debver[:-2],
              'deb_packages': ['apt-transport-https',  # enable HTTPS \
                               'ca-certificates',      # for Debian repos
                               'locales',              # install locale-gen
                               'openssh-server'],      # vagrant requires SSH
              'boostrapscript': os.path.join(workdir,
                                             'fdroidserver-customize.sh'),
              'username': 'vagrant',
              'password': 'vagrant',
              'boxfile': 'basebox-{debver}-{provider}.box'.format(
                  debver=debver, provider=provider),
              'verbose': verbose,
              'dry_run': dry_run}
    params['size_bytes'] = vm_size_str_to_bytes(params['size'])
    params['size_megs'] = math.ceil(params['size_bytes'] / (2.**20))
    params['size_gigs'] = math.ceil(params['size_bytes'] / (2.**30))
    params['img_name_raw'] = params['vm_name'] + '.raw'
    params['img_path_raw'] = os.path.join(workdir, params['img_name_raw'])
    params['vagrantfile_path'] = os.path.join(workdir, 'Vagrantfile')
    # add some packages which are missing in makebuildserver:
    # (because they have been preinstalled on jessie64.box)
    params['deb_packages'] += ('perl',
                               'git',
                               'git-man',
                               'libcurl3-gnutls',
                               'liberror-perl',
                               'libexpat1',
                               'libldap-2.4-2',
                               'librtmp1',
                               'libsasl2-2',
                               'libsasl2-modules-db',
                               'libssh2-1',
                               'libjs-excanvas',
                               'libpython-stdlib',
                               'libpython2.7-minimal',
                               'libpython2.7-stdlib',
                               'libsqlite3-0',
                               'mercurial-common',
                               'mime-support',
                               'python',
                               'python-minimal',
                               'python2.7',
                               'python2.7-minimal')

    if provider == 'libvirt':
        params['img_name_qcow2'] = params['vm_name'] + '.qcow2'
        params['img_path_qcow2'] = os.path.join(workdir,
                                                params['img_name_qcow2'])
        params['metadata_json_path'] = os.path.join(workdir, 'metadata.json')
    elif provider == 'virtualbox':
        params['img_name_vmdk'] = params['vm_name'] + '.vmdk'
        params['img_path_vmdk'] = os.path.join(workdir,
                                               params['img_name_vmdk'])
        params['ovf_path'] = os.path.join(workdir, 'box.ovf')
    return params


def write_bootstrap_script(params, provider):
    if not params['dry_run']:
        path = params['boostrapscript']
        with open(path, 'w', encoding='utf-8') as f:
            f.write(get_resource_as_string('debootstrap-custom.sh')
                    .format(username=params['username'],
                            deb_mirror=params['deb_mirror'],
                            deb_distro=params['deb_distro'],
                            verbose=params['verbose'],
                            provider=provider))
        st = os.stat(path)
        os.chmod(path, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
    else:
        logging.info("(dry run) Skip writing bootstrap script to workdir.")


def exec_vmdebootstrap(params, provider):

    write_bootstrap_script(params,
                           provider)

    cmd = [shutil.which('vmdebootstrap') or 'vmdebootstrap',
           '--grub',
           '--image={}'.format(params['img_path_raw']),
           '--size={}'.format(params['size']),
           '--mirror={}'.format(params['deb_mirror']),
           '--distribution={}'.format(params['deb_distro']),
           '--hostname={}'.format(params['hostname']),
           '--enable-dhcp',
           '--user={}/{}'.format(params['username'], params['password']),
           '--package={}'.format(','.join(params['deb_packages'])),
           '--customize={}'.format(params['boostrapscript']),
           '--lock-root-password',
           '--systemd-network',
           '--sudo',
           '--sparse']
    if params['verbose']:
        cmd += ['--verbose']
    if provider == 'virtualbox':
        cmd += ['--debootstrapopts=components=main,contrib']
    logging.debug('> {}'.format(' '.join(cmd)))
    if not params['dry_run']:
        logging.info('running vmdebootstrap (please be patient, '
                     'this may take several minutes) ...')
        subprocess.check_call(cmd)
    else:
        logging.info("(dry run) Skip bootstraping to Debian "
                     "to raw disk image.")


def libvirt_convert_raw_to_qcow2(params):
    cmd = (shutil.which('qemu-img') or 'qemu-img',
           'convert',
           '-f', 'raw',
           '-O', 'qcow2',
           params['img_path_raw'],
           params['img_path_qcow2'])
    logging.debug('> {}'.format(' '.join(cmd)))
    if not params['dry_run']:
        if os.path.isfile(params['img_path_qcow2']):
            os.remove(params['img_path_qcow2'])
            logging.debug('removed already existing file: {}'.format(
                params['img_path_qcow2']))
        logging.info('converting raw image to qcow2 ...')
        subprocess.check_call(cmd)
    else:
        logging.info('(dry run) Skip converting raw image to qcow2')


def libvirt_write_metadata_json(params):
    if not params['dry_run']:
        with open(params['metadata_json_path'], 'w', encoding="utf-8") as f:
            f.write(json.dumps({'provider': 'libvirt',
                                'format': 'qcow2',
                                'virtual_size': str(params['size_gigs'])},
                               indent=2))
    else:
        logging.info("(dry run) skipped generating '{}'"
                     .format(params['metadata_json_path']))


def libvirt_write_vagrantfile(params):
    if not params['dry_run']:
        with open(params['vagrantfile_path'], 'w', encoding='utf-8') as f:
            f.write(get_resource_as_string('libvirt.Vagrantfile')
                    .format(**params))
    else:
        logging.info("(dry run) skipped generating '{}'"
                     .format(params['vagrantfile_path']))


def libvirt_package_box(params):
    if not params['dry_run']:
        logging.info("building vagrant box file: '{boxfile}' "
                     "(please be patient, this may take several minutes)"
                     .format(**params))
        with tarfile.open(params['boxfile'], 'w:gz') as f:
            logging.debug('packaging box file (metadata.json) ...')
            with open(params['metadata_json_path'],
                      'r', encoding='utf-8') as mf:
                logging.debug('contents:\n{}'.format(mf.read()))
            f.add(params['metadata_json_path'], arcname='metadata.json')
            logging.debug('packaging box file (Vagrantfile) ...')
            with open(params['vagrantfile_path'], 'r', encoding='utf-8') as vf:
                logging.debug('contents:\n{}'.format(vf.read()))
            f.add(params['vagrantfile_path'], arcname='Vagrantfile')
            logging.debug('packaging box file (box.img) ...')
            f.add(os.path.join(os.getcwd(), params['img_path_qcow2']),
                  arcname='box.img')
    else:
        logging.info('(dry run) Skip packaging box file.')


def get_resource_as_string(resource_name):
    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(path, 'resource', resource_name)
    logging.debug("loading resource '{}'".format(resource_name))
    with open(path, 'r', encoding='utf-8') as f:
        return f.read()


def vbox_convert_raw_to_vmdk(params):
    cmd = (shutil.which('VBoxManage') or 'VBoxManage',
           'convertfromraw',
           params['img_path_raw'],
           params['img_path_vmdk'],
           '--format',
           'VMDK')
    logging.debug('> {}'.format(' '.join(cmd)))
    if not params['dry_run']:
        if os.path.isfile(params['img_path_vmdk']):
            os.remove(params['img_path_vmdk'])
            logging.debug('removed already existing file: {}'.format(
                params['img_path_vmdk']))
        logging.info('converting raw image to vmdk ...')
        subprocess.check_call(cmd)
    else:
        logging.info('(dry run) Skip converting raw image to vmdk')


def vbox_write_ovf(params):
    ovf_template = get_resource_as_string('virtualbox.box.ovf')

    if not params['dry_run']:
        with open(params['ovf_path'], 'w', encoding='utf-8') as f:
            f.write(ovf_template.format(**params))
    else:
        logging.info("(dry run) skipped generating '{}'"
                     .format(params['ovf_path']))


def vbox_write_vagrantfile(params):
    if not params['dry_run']:
        with open(params['vagrantfile_path'], 'w', encoding='utf-8') as f:
            f.write(get_resource_as_string('virtualbox.Vagrantfile')
                    .format(**params))
    else:
        logging.info("(dry run) skipped generating '{}'"
                     .format(params['vagrantfile_path']))


def vbox_package_box(params):
    if not params['dry_run']:
        logging.info("building vagrant box file: '{boxfile}' "
                     "(please be patient, this may take several minutes)"
                     .format(**params))
        with tarfile.open(params['boxfile'], 'w:gz') as f:
            logging.debug('packaging box file (Vagrantfile) ...')
            with open(params['vagrantfile_path'], 'r', encoding='utf-8') as vf:
                logging.debug('contents:\n{}'.format(vf.read()))
            f.add(params['vagrantfile_path'], arcname='./Vagrantfile')
            logging.debug('packaging box file (box-disk1.vmdk) ...')
            f.add(os.path.join(os.getcwd(), params['img_path_vmdk']),
                  arcname='./box-disk1.vmdk')
            logging.debug('packaging box file (box.ovf) ...')
            with open(params['ovf_path'], 'r', encoding='utf-8') as mf:
                logging.debug('contents:\n{}'.format(mf.read()))
            f.add(params['ovf_path'], arcname='./box.ovf')
    else:
        logging.info('(dry run) Skip packaging box file.')


def main(provider='virtualbox', debver='stretch64', workdir=None,
         verbose=False, dry_run=False, skip_checks=False):

    if not os.geteuid() == 0 and not skip_checks:
        raise BaseboxException('This script requires super user privileges. '
                               '(Please use sudo or run as root.)')

    if provider not in SUPPORTED_PROVIDERS:
        providers = ', '.join(SUPPORTED_PROVIDERS)
        raise BaseboxException("provider '{selected}' not supported. "
                               "Supported providers are: {supported}"
                               .format(selected=provider,
                                       supported=providers))
    logging.info("selected provider: '{}'".format(provider))

    if debver not in SUPPORTED_DEBVERS:
        supvers = ', '.join(SUPPORTED_DEBVERS)
        raise BaseboxException("target debian version '{debver}' not "
                               "supported. Supported versions are: "
                               "{supvers}".format(debver=debver,
                                                  supvers=supvers))

    if not shutil.which('vmdebootstrap') and not skip_checks:
        raise BaseboxException("Can not find 'vmdebootstrap' executable. "
                               "(Please install vmdebootstrap.)")
    if provider == 'virtualbox' and not skip_checks:
        if not shutil.which('VBoxManage'):
            raise BaseboxException("Can not find 'VBoxManage' executable. "
                                   "(Please install virtualbox.)")
    elif provider == 'libvirt' and not skip_checks:
        if not shutil.which('qemu-img'):
            raise BaseboxException("Can not find 'qemu-img' executable. "
                                   "(Please install qemu-utils.)")

    if provider == 'virtualbox' and debver == 'stretch64' and not skip_checks:
        if not shutil.which('systemd-nspawn'):
            raise BaseboxException("Can not find 'systemd-nspawn' executable. "
                                   "(Please install systemd-container.)")

    with tempfile.TemporaryDirectory() as tmpdir:

        if not workdir:
            workdir = tmpdir

        # main parameters for this image
        params = init_params(provider, debver, workdir=workdir,
                             verbose=verbose, dry_run=dry_run)
        logging.debug('image parameters: {}'
                      .format(json.dumps(params, indent=2, sort_keys=True)))

        exec_vmdebootstrap(params, provider)

        if provider == 'libvirt':
            libvirt_convert_raw_to_qcow2(params)
            libvirt_write_metadata_json(params)
            libvirt_write_vagrantfile(params)
            libvirt_package_box(params)
        elif provider == 'virtualbox':
            vbox_convert_raw_to_vmdk(params)
            vbox_write_ovf(params)
            vbox_write_vagrantfile(params)
            vbox_package_box(params)


if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument("-v", "--verbose", action="store_true", default=False,
                        help="Spew out even more information than normal")
    parser.add_argument("-q", "--quiet", action="store_true", default=False,
                        help="Restrict output to warnings and errors")
    parser.add_argument('-t', '--target', default="stretch64",
                        help="target debian version. "
                             "defaults to 'stretch64'. "
                             "(supported: "
                             ', '.join(SUPPORTED_DEBVERS) + ')')
    parser.add_argument('-p', '--provider', default='virtualbox',
                        help="target vagrant provider. "
                             "defaults to 'virtualbox'. "
                             "(supported: "
                             ', '.join(SUPPORTED_PROVIDERS) + ")")
    parser.add_argument('-d', '--dry-run', action='store_true', default=False,
                        help='don\'t actually bulild the image')
    parser.add_argument('--workdir', default=None,
                        help="put intermediate steps into requested path, "
                             "instead of temporary dir")
    parser.add_argument('-S', '--skip-checks',
                        action='store_true', default=False,
                        help="do not check for dependencies, "
                        "user permissions, etc.")
    options = parser.parse_args()

    # Helpful to differentiate warnings from errors even when on quiet
    logformat = '%(levelname)s: %(message)s'
    loglevel = logging.INFO
    if options.verbose:
        loglevel = logging.DEBUG
    elif options.quiet:
        loglevel = logging.WARN
    logging.basicConfig(format=logformat, level=loglevel)

    try:
        main(dry_run=options.dry_run,
             provider=options.provider,
             debver=options.target,
             verbose=options.verbose,
             workdir=options.workdir,
             skip_checks=options.skip_checks)
    except BaseboxException as e:
        logging.critical(e)
        exit(1)
